const request = require('request')

const client = () => ({
    gameMode : `${process.env.GAME_MODE}`,
    pseudo :   `${process.env.PSEUDO}`,
    token :    `${process.env.TOKEN}`,

    uriBase : () => {
        return (mode === 'test') ? `${process.env.API_TEST_URL}` : `${process.env.API_URL}`
    },

    connect : () => {
        if (mode === 'test') {
            call(uriBase() + 'connect', pseudo) // @TODO : format response with Typescript
            token = '...'
        } else {
            call(uriBase() + 'connect', token) // @TODO : format response with Typescript
            pseudo = '...'
        }

        console.log(pseudo + ' est connecté avec le token ' + token)
        console.log('La partie commence dans ' + '($response->getWait() / 1000)' + ' secondes')

        //sleep
    },

    infos : () => {
        call(uriBase() + 'info', token) // @TODO : format response with Typescript
    },

    directions : (turn) => {
        call(uriBase() + 'directions', token, turn) // @TODO : format response with Typescript
    },

    move : (direction, turn) => {
        call(uriBase() + 'move', token, direction, turn) // @TODO : format response with Typescript
    },

    async call(...args) {
        request.get(Array.join(args))
    }
})

module.exports = {
    client
}
