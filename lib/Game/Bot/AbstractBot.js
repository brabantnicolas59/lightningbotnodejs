const LightningBot = require('../../Client/LightningBotClient')

class AbstractBot {

    constructor() {

    }

    activation() {
        LightningBot.connect()

        try {
            this.getInfos()

            turn = 1
            while (true) {
                console.log('current turn : ' + turn)

                waitForNextTurn = this.getDirections(turn);

                waitForNextTurn = this.defineNextMove(turn, waitForNextTurn);

                console.log((waitForNextTurn / 1000) + ' seconds until next turn.');

                turn++
            }
        } catch (exception) {
            console.error(exception)
        }
    }

    getInfos() {
        //console.log('lets go!')
        //console.log(`Your port is ${process.env.API_URL}`);

        LightningBot.client.infos()

        this.receiveInformation()


        setTimeout(function(){

        }, 2000);
        //let wait =
    }

    getDirections(turn) {
        LightningBot.client.directions(turn)
    }

    defineNextMove(turn, waitForNextTurn) {
        LightningBot.client.move()
    }

    receiveInformation(infoResponse) {
        throw new Error('You must implement this function');
    }

    receiveDirections(directionsResponse, turn) {
        throw new Error('You must implement this function');
    }

    chooseDirection(turn, waitForNextTurn) {
        throw new Error('You must implement this function');
    }
}

module.exports = AbstractBot
