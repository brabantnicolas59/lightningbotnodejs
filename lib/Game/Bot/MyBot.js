const AbstractBot = require('./AbstractBot')

class MyBot extends AbstractBot {

    /**
     * Cette fonction est appelée une seule fois lors de l'initialisation de la partie
     * Elle permet la récupération d'informations utiles à exploiter
     *
     * @param \App\Client\Response\InfoResponse $infoResponse
     *
     * @return mixed
     */
    receiveInformation(infoResponse) {
        throw new Error('You must implement this function');
    }

    /**
     * Cette fonction est appelée à chaque tour et permet la récupération
     * des directions qu'on pris les autres joueurs au tour précédent
     *
     * @param \App\Client\Response\DirectionsResponse $directionsResponse
     * @param int $turn
     *
     * @return mixed
     */
    receiveDirections(directionsResponse, turn) {
        throw new Error('You must implement this function');
    }

    /**
     * Cette fonction est appelée une fois par tour et permet d'indiquer au serveur
     * la direction à prendre pour le tour en cours
     *
     * @param int $turn
     * @param $waitForNextTurn
     *
     * @return int
     */
    chooseDirection(turn, waitForNextTurn) {
        throw new Error('You must implement this function');
    }
}

module.exports = new MyBot()
