const dotenv = require('dotenv');
const { GameManager } = require('../lib/Game/GameManager');

dotenv.config();

GameManager.start()
